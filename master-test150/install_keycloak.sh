#!/bin/bash
#
# installs keycloak service as pod from adc-middleware 
# see https://github.com/ireceptorplus-inesctec/adc-middleware
#
# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh" 


if [ ! -f "$SCRIPT_DIR/$POD_CONF_FILE" ]; then
    echo "The file $POD_CONF_FILE does not exist. Edit file podman-adcauth.conf.sh.EDIT then remove .EDIT"
	exit
fi

# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE 

#echo --- BIN_PODMAN  ---  $PODMAN_CMD
log "[$KEYCL_POD_NAME_SVC installation]"
log "pod service name: $KEYCL_POD_NAME_SVC"
log "pod external port: $KEYCL_SVC_PORT"
log "user id running db container: $POD_USER_ID"

# required commands
commands=("podman" "curl" "readlink" "buildah");
for i in "${commands[@]}"
do
	if ! [ $(command -v $i) > 0 ];then
		log "	$i command could not be found, install first. "
		exit
	else
		log "	$i command available"		
	fi
done

# db data folder
KEYCL_DB_DATA_DIR=".keycloak_db_data"
log "create $KEYCL_DB_DATA_DIR"
if [ ! -e $KEYCL_DB_DATA_DIR  ];then

	mkdir $KEYCL_DB_DATA_DIR >> $SCRIPT_DIR/$LOGFILE_NAME 
	podman unshare chown $POD_USER_ID:$POD_USER_ID -R $KEYCL_DB_DATA_DIR 
else
	log "	data directory for db already exists"
	exit
fi

log "check for existing pod.."
podman pod exists $KEYCL_POD_NAME_SVC 
if [ $? -eq 0  ]; then
	log "	pod with that name already exists."
	exit
fi

# --- pull containers ---------------------------------------------------------

# build container image
# https://hub.docker.com/r/jboss/keycloak/tags
# docker pull jboss/keycloak:15.0.0
# Dockerfile
# FROM jboss/keycloak:15.0.0
# COPY ./js-policies /opt/jboss/keycloak/standalone/deployments
podman pull registry.hub.docker.com/jboss/keycloak:${KEYCL_SVC_CONT_TAG} 
sleep 2
# postgres:12

podman pull docker.io/library/postgres:${KEYCL_DB_CONT_TAG} 
sleep 2

# --- pod create --------------------------------------------------------------

log "creating the keycloak pod.."
podman pod create --name $KEYCL_POD_NAME_SVC --share net -p $KEYCL_POD_EX_PORT_HTTP:8080 -p $KEYCL_POD_EX_PORT_DB:5432 

log "add container postgres (keycloak_db)"
podman run --user $POD_USER_ID -d --pod=$KEYCL_POD_NAME_SVC  \
            -e POSTGRES_DB="keycloak_db" \
			-e POSTGRES_PASSWORD="password2" \
			-v $KEYCL_DB_DATA_DIR:/var/lib/postgresql/data:Z \
            --name=keycloak_db${KEYCL_DB_CONT_NAME_SUFFIX} \
			docker.io/library/postgres:$KEYCL_DB_CONT_TAG

wait "run keycloak_db" 10

# https://hub.docker.com/r/jboss/keycloak
#-e KEYCLOAK_IMPORT="/tmp/realm-export.json" \
log "add container keycloak service"
podman run --user $POD_USER_ID -d --pod=$KEYCL_POD_NAME_SVC  \
            -e DB_VENDOR="postgres" \
			-e DB_ADDR="localhost" \
			-e DB_DATABASE="keycloak_db" \
			-e DB_USER="postgres" \
			-e DB_PASSWORD=$KEYCL_DB_PASSWORD \
			-e KEYCLOAK_USER=$KEYCL_ADMIN_USER \
			-e KEYCLOAK_PASSWORD=$KEYCL_ADMIN_PASSWORD \
			-e DB_SCHEMA="public" \
			-e PROXY_ADDRESS_FORWARDING="true" \
			-v keycloak:/tmp:Z \
            --name=keycloak${KEYCL_SVC_CONT_NAME_SUFFIX} \
			registry.hub.docker.com/jboss/keycloak:${KEYCL_SVC_CONT_TAG}

wait "run keycloak service" 10

# --- create systemd service --------------------------------------------------

# launch on boot
# adc-middleware/pod-adc-auth-keycloak-test1.service
# adc-middleware/container-keycloak-test1.service
# adc-middleware/container-keycloak_db-test1.service

mkdir -p ~/.config/systemd/user/

#podman generate systemd --name $POD_NAME_SVC > ~/.config/systemd/user/pod-${POD_NAME_SVC}.service
#systemctl enable pod-${POD_NAME_SVC}.service --user
podman generate systemd --files --name $KEYCL_POD_NAME_SVC

# move file to user directory for systemd 
cp pod-${KEYCL_POD_NAME_SVC}.service  ~/.config/systemd/user/pod-${KEYCL_POD_NAME_SVC}.service
rm pod-${KEYCL_POD_NAME_SVC}.service

cp container-keycloak_db${KEYCL_DB_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-keycloak_db${KEYCL_DB_CONT_NAME_SUFFIX}.service
rm container-keycloak_db${KEYCL_DB_CONT_NAME_SUFFIX}.service

cp container-keycloak${KEYCL_SVC_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-keycloak${KEYCL_SVC_CONT_NAME_SUFFIX}.service
rm container-keycloak${KEYCL_SVC_CONT_NAME_SUFFIX}.service

sleep 2
systemctl enable pod-${KEYCL_POD_NAME_SVC}.service --user

log "systemd service files created and service enabled"

wait "processes to start" 20
echo " "
# --- pod top -----------------------------------------------------------------

# log "running processes in pod"
podman pod top $KEYCL_POD_NAME_SVC

#log "stopping running pod ..."
#podman pod stop $KEYCL_POD_NAME_SVC

#wait "stopping running pod ... " 30

#log "starting systemd service ..."
#systemctl --user start pod-${KEYCL_POD_NAME_SVC}.service

#wait "service starting ..." 30

echo "keycloak service runs as a user service"
echo "check service : systemctl status pod-${KEYCL_POD_NAME_SVC}.service --user"
echo "start service : systemctl start pod-${KEYCL_POD_NAME_SVC}.service --user"
echo "stop service     : systemctl stop pod-${KEYCL_POD_NAME_SVC}.service --user"
echo "disable service : systemctl disable pod-${KEYCL_POD_NAME_SVC}.service --user"
echo "enable service : systemctl enable pod-${KEYCL_POD_NAME_SVC}.service --user"
echo 

echo "end of installation, keycloak management interface is available on local port http://${KEYCL_HOST}:${KEYCL_POD_EX_PORT_HTTP}, you can find more information here https://www.keycloak.org/documentation.html"
log "end of keycloak installation, next step install adc-middleware servcice"

