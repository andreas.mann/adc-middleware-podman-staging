#!/bin/bash
#
# installs middleware service as pod from adc-middleware 
# see https://github.com/ireceptorplus-inesctec/adc-middleware
#
# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh" 


if [ ! -f "$SCRIPT_DIR/$POD_CONF_FILE" ]; then
    echo "The file $POD_CONF_FILE does not exist. Edit file podman-adcauth.conf.sh.EDIT then remove .EDIT"
	exit
fi

# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE 

#echo --- BIN_PODMAN  ---  $PODMAN_CMD
log "[$MIDWARE_POD_NAME_SVC installation]"
log "pod service name: $MIDWARE_POD_NAME_SVC"
log "pod external port: $MIDWARE_SVC_PORT"
log "user id running db container: $POD_USER_ID"

# required commands
commands=("podman" "curl" "readlink" "buildah");
for i in "${commands[@]}"
do
	if ! [ $(command -v $i) > 0 ];then
		log "	$i command could not be found, install first. "
		exit
	else
		log "	$i command available"		
	fi
done

# db data folder
MIDWARE_DB_DATA_DIR=".midware_db_data"
log "create $MIDWARE_DB_DATA_DIR"
if [ ! -e $MIDWARE_DB_DATA_DIR  ];then

	mkdir $MIDWARE_DB_DATA_DIR >> $SCRIPT_DIR/$LOGFILE_NAME 
	podman unshare chown $POD_USER_ID:$POD_USER_ID -R $MIDWARE_DB_DATA_DIR 
else
	log "	data directory for db already exists"
	exit
fi

# redis db data folder
MIDWARE_DB_REDIS_DATA_DIR=".midware_db_redis_data"
log "create $MIDWARE_DB_REDIS_DATA_DIR"
if [ ! -e $MIDWARE_DB_REDIS_DATA_DIR  ];then

    mkdir $MIDWARE_DB_REDIS_DATA_DIR >> $SCRIPT_DIR/$LOGFILE_NAME 
    podman unshare chown $POD_USER_ID:$POD_USER_ID -R $MIDWARE_DB_REDIS_DATA_DIR 
else
    log "   data directory for db redis already exists"
    exit
fi


log "check for existing pod.."
podman pod exists $MIDWARE_POD_NAME_SVC 
if [ $? -eq 0  ]; then
	log "	pod with that name already exists."
	exit
fi

# --- pull containers ---------------------------------------------------------

# build container image
# https://hub.docker.com/r/jboss/keycloak/tags
# docker pull jboss/keycloak:15.0.0
# Dockerfile
# FROM jboss/keycloak:15.0.0
# COPY ./js-policies /opt/jboss/keycloak/standalone/deployments
# podman pull registry.hub.docker.com/jboss/keycloak:${KEYCL_SVC_CONT_TAG} 
# docker pull irpinesctec/adc-middleware:1.5.0
# docker pull irpinesctec/keycloak_extension_api:latest
podman pull registry.hub.docker.com/irpinesctec/adc-middleware:$MIDWARE_SVC_CONT_TAG
sleep 2

podman pull docker.io/library/postgres:$MIDWARE_DB_CONT_TAG
sleep 2

podman pull docker.io/redis:$MIDWARE_REDIS_CONT_TAG
sleep 2

podman pull registry.hub.docker.com/irpinesctec/keycloak_extension_api:$MIDWARE_KEYCL_EXT_API_CONT_TAG
sleep 2

# --- pod create --------------------------------------------------------------

log "creating the adc-middleware pod.."
podman pod create --name $MIDWARE_POD_NAME_SVC --share net -p $MIDWARE_POD_EX_PORT_HTTP:8080 -p $MIDWARE_POD_EX_PORT_DB:5432

# --- start and add containers to pod ----------------------------------------- 

log "add container redis"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			-v $MIDWARE_DB_REDIS_DATA_DIR:/data:Z \
			--name=middleware-redis$MIDWARE_REDIS_CONT_NAME_SUFFIX \
			docker.io/library/redis:$MIDWARE_REDIS_CONT_TAG

wait "run redis" 3

log "add keycloac extension api"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			-e DB_DATABASE="keycloak_db" \
			-e DB_USER="postgres" \
			-e DB_PASSWORD=$KEYCL_DB_PASSWORD \
			-e DB_HOST=$KEYCL_DB_HOSTNAME \
			-e DB_PORT=$KEYCL_POD_EX_PORT_DB \
			-e KEYCLOAK_URL=$KEYCL_SVC_URL \
			-e REALM=$KEYCL_REALM \
			--name=keycloakextensionapi$MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX \
			registry.hub.docker.com/irpinesctec/keycloak_extension_api:$MIDWARE_KEYCL_EXT_API_CONT_TAG

wait "run keycl ext api" 3

log "add container postgres (midware_db)"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			-e POSTGRES_PASSWORD=$MIDWARE_DB_PASSWORD \
			-v $MIDWARE_DB_DATA_DIR:/var/lib/postgresql/data:Z \
            --name=middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX} \
			docker.io/library/postgres:$MIDWARE_DB_CONT_TAG

wait "run postgres" 3

log "add container adc-middleware service"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC --add-host=${KEYCL_HOST}:${KEYCL_HOST_IP}  \
			-e UMA_CLIENT_SECRET=$MIDWARE_UMA_CLIENT_SECRET \
			-e DB_PASSWORD=$MIDWARE_DB_PASSWORD \
			-e PROPERTIES_PATH="/config/middleware/${MIDWARE_PROPS_FILE}" \
			-v ./${MIDWARE_CONFIG_FOLDER}:/config:Z \
			-v ./src/main/resources/db/postgresql:/config/db/postgresql:Z \
            --name=middleware${MIDWARE_SVC_CONT_NAME_SUFFIX} \
			registry.hub.docker.com/irpinesctec/adc-middleware:$MIDWARE_SVC_CONT_TAG

wait "run middleware" 4

# --- create systemd service --------------------------------------------------

# launch on boot
# adc-middleware/pod-adc-auth-keycloak-test1.service
# adc-middleware/container-keycloak-test1.service
# adc-middleware/container-keycloak_db-test1.service

mkdir -p ~/.config/systemd/user/

#podman generate systemd --name $POD_NAME_SVC > ~/.config/systemd/user/pod-${POD_NAME_SVC}.service
#systemctl enable pod-${POD_NAME_SVC}.service --user
podman generate systemd --files --name $MIDWARE_POD_NAME_SVC

# pod service move file to user directory for systemd 
cp pod-${MIDWARE_POD_NAME_SVC}.service  ~/.config/systemd/user/pod-${MIDWARE_POD_NAME_SVC}.service
rm pod-${MIDWARE_POD_NAME_SVC}.service

# redis container
cp container-middleware-redis${MIDWARE_REDIS_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware-redis${MIDWARE_REDIS_CONT_NAME_SUFFIX}.service
rm container-middleware-redis${MIDWARE_REDIS_CONT_NAME_SUFFIX}.service

# kc extension api container
cp container-keycloakextensionapi${MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-keycloakextensionapi${MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX}.service
rm container-keycloakextensionapi${MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX}.service

# middleware_db container
cp container-middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX}.service
rm container-middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX}.service

# middleware service container
cp container-middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}.service
rm container-middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}.service

wait "enable systemd service" 2
systemctl enable pod-${MIDWARE_POD_NAME_SVC}.service --user

log "systemd service files created and service enabled"

wait "processes to start" 30

# --- pod top -----------------------------------------------------------------

# log "running processes in pod"
podman pod top $MIDWARE_POD_NAME_SVC

#podman pod stop $MIDWARE_POD_NAME_SVC

#wait "stopping running pod and start systemd service" 30

#systemctl start pod-${MIDWARE_POD_NAME_SVC}.service --user
#wait "service starting ..." 15

echo "adc-middleware service runs as a user service"
echo "check service : systemctl status pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "start service : systemctl start pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "stop service     : systemctl stop pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "disable service : systemctl disable pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "enable service : systemctl enable pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo 

#echo "end of installation for adc-middleware"
log "End of adc-middleware installation, next step configure integration with keycloak"

