#!/bin/bash
# -----------------------------------------------------------------------------
# steps:
# (1) ...
#
# -----------------------------------------------------------------------------


#if (( $EUID != 0 )); then
    #echo "Please run with sudo.."
    #exit
#fi

# service admin e-mail
sysadmin_email="noreply@example.com";

# only used in development 
IP_LAN_DEVICE="vpn0"

# hostname 
host_name=$(hostname);

# path to backup directory, shared by all pods on host
# this pod will have its own dir inside that path 
PATH_BKUP_DIR="/var/data/bkup";

# if set to 1, bkup_file_rotation.sh will only use backup.daily folder
# by default, files are kept for 14 days in that folder, see script for details
BKUP_USES_ONLY_DIR_DAILY="1";

# -----------------------------------------------------------------------------
# keyloak and adc-middleware configuration
GLOBAL_INSTANCE_SUFFIX="-LEGOLAS-dev"

# -----------------------------------------------------------------------------
# keycloak pod configuration

# hostname where keycloak is available, if both pods are installed on same host and hostname
# resolution is not possible from middleware container, KEYCL_HOST:KEYCL_HOST_IP will be 
# added to pod, see install script line 122
KEYCL_HOST="legolas" 

# ipv4 of keycloak host , used in middleware svc pod hosts file
KEYCL_HOST_IP="127.0.0.1"

# used to identify individual pods and containers
KEYCL_INSTANCE_SUFFIX=$GLOBAL_INSTANCE_SUFFIX

# suffix to specify keycloak container insance e.g. TEST, DEV, PROD
KEYCL_SVC_CONT_NAME_SUFFIX=$KEYCL_INSTANCE_SUFFIX

# suffix to specify keycloak db container insance e.g. TEST, DEV, PROD
KEYCL_DB_CONT_NAME_SUFFIX=$KEYCL_INSTANCE_SUFFIX

# TAG for built svc  container 
KEYCL_SVC_CONT_TAG="15.0.0"

# TAG for built db container (postgress)
KEYCL_DB_CONT_TAG="12"

# service name for keycloak pod
KEYCL_POD_NAME_SVC="adc-keycloak${KEYCL_INSTANCE_SUFFIX}"

# external port of keycloak pod http service
KEYCL_POD_EX_PORT_HTTP="8081"

# keycloak initial user for admin console
KEYCL_ADMIN_USER="admin"

# keycloak initial password for admin user
KEYCL_ADMIN_PASSWORD="admin"

# password for keycloak db user postgres
KEYCL_DB_PASSWORD="password2"

# host port of keycloak postgres db
KEYCL_POD_EX_PORT_DB="5432"

# used by keycloak extension api

# the keycloak extension api container needs to connect to keycloak db host on postgres port, see KEYCL_POD_EX_PORT_DB
# add hostname where keycloak is available, if both pods are installed on same host and hostname
# resolution is not possible from middleware container, KEYCL_HOST:KEYCL_HOST_IP will be
# added to pod, see install script line 122, in that case use KEYCL_HOST as default value
KEYCL_DB_HOSTNAME=$KEYCL_HOST

# pod running the keycloak extension api needs to contact keycloak service host
KEYCL_SVC_URL="http://${KEYCL_HOST}:${KEYCL_POD_EX_PORT_HTTP}/auth/"

# keycloak realm, which manages objects for this turnkey deyployment. used by keycloak extension api 
KEYCL_REALM="realm${GLOBAL_INSTANCE_SUFFIX}"

# -----------------------------------------------------------------------------
# middleware pod configuration
# -----------------------------------------------------------------------------
# follow the steps below:
# (1) find a place where adc middleware should be installed, we use /var/data on our host
# $ git clone https://github.com/ireceptorplus-inesctec/adc-middleware.git  
# adjust permissions so that the running user account can read and write in adc-middleware
# (2) cd into adc-middleware
# copies example content to working folder
# $ cp -r example/ working
# make a copy of the properties file for your own settings
# $ cp working/config/middleware/example.properties working/config/middleware/middleware.properties
# in section below set MIDWARE_CONFIG_FOLDER if you did not use working as folder name
# in file middleware.properties
# change at least:
# -- adc.resourceServerUrl - FQDN to your adc compliant repository, adjust host part
# -- server.port - change value to 8080
# -- app.resourceAllowedOrigins="" - add that configuration property
# -- uma.wellKnownUrl  - adjust host and port where your keycloak service is available (host and port)
# -- spring.redis.host - adjust value to "localhost" (runs in same pod)
# (3) read configuration below and adjust values if required for your deployment 

# top level folder for adc-middleware configuration files
# if you copied example to working do not change value  
MIDWARE_CONFIG_FOLDER="working/config"

# uses same file path than example.properties
# if you did like before dont change value
MIDWARE_PROPS_FILE="middleware.properties"

# dont change the following five values

# added to adc-middleware and keycloak pod and container names
MIDWARE_INSTANCE_SUFFIX=$GLOBAL_INSTANCE_SUFFIX

# makes api container unique for this installation
MIDWARE_SVC_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# suffix to specify middleware db container insance e.g. TEST, DEV, PROD
MIDWARE_DB_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# name suffix for redis container
MIDWARE_REDIS_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# name suffix for keycloak extension api
MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# TAG for built svc  container https://hub.docker.com/r/irpinesctec/adc-middleware/tags
MIDWARE_SVC_CONT_TAG="1.7.0"

# TAG for built db container (postgres)
MIDWARE_DB_CONT_TAG="12"

# service name for middleware pod
MIDWARE_POD_NAME_SVC="adc-middleware${KEYCL_INSTANCE_SUFFIX}"

# password for middleware db user postgres
MIDWARE_DB_PASSWORD="password2"

# host port of middleware postgres db, if postgres keycloak db runs on host port 5432, use 5333 here
MIDWARE_POD_EX_PORT_DB="5433"

# endpoint for protected adc api, e.g. for unprotected 8445 use 9445 for development deployment
MIDWARE_POD_EX_PORT_HTTP="9445"

# establishes trust relationship with keycloak service
MIDWARE_UMA_CLIENT_SECRET="c2b7e035-9fe7-41de-83c0-2561b713ed91"

# https://hub.docker.com/_/redis?tab=tags, leave empty or latest 
MIDWARE_REDIS_CONT_TAG="latest"

# tag for keycloakextensionapi 
# https://hub.docker.com/r/irpinesctec/keycloak_extension_api/tags
MIDWARE_KEYCL_EXT_API_CONT_TAG="0.4.0"

# -----------------------------------------------------------------------------
# POST keycloak installation steps
# (1) after keycloak installation, login to admin console
# (2) create realm with value of KEYCL_REALM an turn on "User-Managed Access" on general tab
# (3) add adc-middleware client to keycloak: clients "create", import adc-middleware.json from working/config/keycloak folder 
# (4) add generated secret from clients "Credentials" tab to adc-middleware config in a) this file b) middleware.properties 
#	and c) postman, if used
# (5) add user account "owner" (set non temporary password in Credentials) with "synchronize" role group membership  
# (6) add normal user accounts in keycloak client as required  
# (7) install adc-middleware pod

# -----------------------------------------------------------------------------
# service account id running the pod 
POD_USER_ID="1000"

# existing file will trigger db backup
# created by load_ and update_ scripts
DO_BKUP_FLAG=${PATH_BKUP_DIR}/${KEYCL_POD_NAME_SVC}/BKUP_DB.FLAG

# -----------------------------------------------------------------------------
SCRIPT_DIR=`dirname "$0"`

CURRENTUSER=$(who | awk 'NR==1{print $1}')
#echo $CURRENTUSER

LOGFILE_NAME="adcauth_${host_name}_${KEYCL_POD_NAME_SVC}_.log"
if [ ! -e $SCRIPT_DIR/$LOGFILE_NAME  ];then
	touch $SCRIPT_DIR/$LOGFILE_NAME
	#chown $CURRENTUSER:$CURRENTUSER $SCRIPT_DIR/$LOGFILE_NAME
fi

function log () {
	local msg=$1
	echo $msg
	local dt=`date +%F_%T`
	echo "$dt $msg" >> $SCRIPT_DIR/$LOGFILE_NAME
}

function wait () {
	arg1=$1
	arg2=$2
	secs=$arg2
	#secs=$((5 * 60))
	while [ $secs -gt 0 ]; do
   		echo -ne "wait $arg2 secs (${arg1}) ... $secs\033[0K\r"
   		sleep 1
   		: $((secs--))
	done
}

function myip () {
	IP_LAN_DEVICE="vpn0"
	current_IPv4_Lan=$(ip -o -4 addr list $IP_LAN_DEVICE | awk '{print $4}' | cut -d/ -f1)
	echo $current_IPv4_Lan
}

log "---"
echo $(myip)
KEYCL_HOST_IP=$(myip)
