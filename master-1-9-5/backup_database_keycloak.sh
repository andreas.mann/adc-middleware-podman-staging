#!/bin/bash

restore_abs_file_path=$1
parent_dir="$(dirname "$restore_abs_file_path")"
base_name="$(basename "$restore_abs_file_path")"


SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE
#echo ${DO_BKUP_FLAG}
if [ -f "${DO_BKUP_FLAG_KEYCL}" ]; then
    log "DO_BKUP_FLAG exists...";
    rm ${DO_BKUP_FLAG_KEYCL}
else 
    exit;
fi

#bkup_file_name=mongodb_${host_name}_${POD_NAME_SVC}_$(date +%s).dump
bkup_file_name=postgresdb_${host_name}_${KEYCL_POD_NAME_SVC}_$(date +%Y%m%dT%H%M%S).sql.dump

#sudo docker-compose --file ${SCRIPT_DIR}/docker-compose.yml --project-name turnkey-service exec -T ireceptor-database \
#   sh -c 'mongodump --archive'
# root@98106decdb80:/mnt/bkup# pg_dump -Fc keycloak_db -U postgres -h localhost > /mnt/bkup/incoming/postgres_legolas_adc-keycloak-LEGOLAS-dev_211206134500.sql.dump

podman exec -u root \
            -e KeycloakDbName="keycloak_db" \
            -e bkup_file_name=$bkup_file_name \
            keycloak_db${KEYCL_DB_CONT_NAME_SUFFIX} \
            sh -c 'pg_dump -Fc $KeycloakDbName -U postgres -h localhost > /mnt/bkup/incoming/$bkup_file_name' 

log "backup db: ${bkup_file_name}"

