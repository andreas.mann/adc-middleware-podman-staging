#!/bin/bash
# -----------------------------------------------------------------------------
# The deployment (with podman) for the adc security framework consists of two   
# pods: a pod running keycloak and another pod running adc-middleware. The pods
# communicate over the hosts ports with each other and the clients from outside.
# steps:
# ...
# adjust the values below according to your deployment plan
# 
# POST keycloak installation steps
# (1) after keycloak installation, login to admin console
# (2) create realm with value of KEYCL_REALM an turn on "User-Managed Access" on general tab
# (3) add adc-middleware client to keycloak: clients "create", import adc-middleware.json from working/config/keycloak folder 
# (4) add generated secret from clients "Credentials" tab to adc-middleware config in a) this file b) middleware.properties 
#	and c) postman, if used
# (5) add user account "owner" (set non temporary password in Credentials) with "synchronize" role group membership  
# (6) add normal user accounts in keycloak client as required  
# (7) install adc-middleware pod
# ..
# -----------------------------------------------------------------------------


#if (( $EUID != 0 )); then
    #echo "Please run with sudo.."
    #exit
#fi

# service admin e-mail
sysadmin_email="noreply@example.com";

# only used in development 
# HO vpn0 Home enp0s25 (you cannot use a device with an ip from 10/24 net because that is the podman network!!!)
# you cannot use lo or 127.0.0.1 here because that ip represents something different from within a pod
IP_LAN_DEVICE="vpn0"

# hostname 
host_name=$(hostname);

# path to backup directory, shared by all pods on host
# this pod will have its own dir inside that path 
PATH_BKUP_DIR="/var/data/bkup";

# if set to 1, bkup_file_rotation.sh will only use backup.daily folder
# by default, files are kept for 14 days in that folder, see script for details
BKUP_USES_ONLY_DIR_DAILY="1";

# -----------------------------------------------------------------------------
# keyloak and adc-middleware configuration
#GLOBAL_INSTANCE_SUFFIX="-LEGOLAS-dev-2022-0215"
GLOBAL_INSTANCE_SUFFIX="-LEGOLAS-dev-195"

# in case your reverse proxy (like nginx) uses custom ca certificates, the files in list below
# will be copied and installed into middleware container  
GLOBAL_DO_UPDATE_CUSTOM_CERTIFICATES="true"

# list of custom root and intermediate certificates, which should be trusted in container
# files must be present in MIDWARE_CONFIG_FOLDER before installation and end with .crt (chmod 644) 
GLOBAL_CUSTOM_CERTIFICATES_LIST=( "intermediate.cert.pem.crt" "ca.cert.pem.crt")

# -----------------------------------------------------------------------------
# keycloak pod configuration

# hostname where keycloak is available, if both pods are installed on same host and hostname
# resolution is not possible from middleware container, KEYCL_HOST:KEYCL_HOST_IP will be 
# added to pod, see install script line 122
KEYCL_HOST="legolas" 

# ipv4 of keycloak host , used in middleware svc pod hosts file, is set below, cannot be 127.0.0.1 or ::1
KEYCL_HOST_IP="127.0.0.1"

# used to identify individual pods and containers
KEYCL_INSTANCE_SUFFIX=$GLOBAL_INSTANCE_SUFFIX

# suffix to specify keycloak container insance e.g. TEST, DEV, PROD
KEYCL_SVC_CONT_NAME_SUFFIX=$KEYCL_INSTANCE_SUFFIX

# suffix to specify keycloak db container insance e.g. TEST, DEV, PROD
KEYCL_DB_CONT_NAME_SUFFIX=$KEYCL_INSTANCE_SUFFIX

# TAG for built svc  container https://hub.docker.com/r/jboss/keycloak/tags 
#KEYCL_SVC_CONT_TAG="15.0.0"
KEYCL_SVC_CONT_TAG="16.1.1"

# TAG for built db container (postgress)
KEYCL_DB_CONT_TAG="12"

# service name for keycloak pod
KEYCL_POD_NAME_SVC="adc-keycloak${KEYCL_INSTANCE_SUFFIX}"

# external port of keycloak pod http service
KEYCL_POD_EX_PORT_HTTP="8081"

# keycloak initial user for admin console
KEYCL_ADMIN_USER="admin"

# keycloak initial password for admin user
KEYCL_ADMIN_PASSWORD="admin"

# password for keycloak db user postgres
KEYCL_DB_PASSWORD="password2"

# host port of keycloak postgres db
KEYCL_POD_EX_PORT_DB="5432"

# used by keycloak extension api

# the keycloak extension api container needs to connect to keycloak db host on postgres port, see KEYCL_POD_EX_PORT_DB
# add hostname where keycloak is available, if both pods are installed on same host and hostname
# resolution is not possible from middleware container, KEYCL_HOST:KEYCL_HOST_IP will be
# added to pod, see install script line 122, in that case use KEYCL_HOST as default value
KEYCL_DB_HOSTNAME=$KEYCL_HOST

# IP of KEYCL_DB_HOSTNAME, is changed at end of this file
KEYCL_DB_IP="127.0.0.1"

# pod running the keycloak extension api needs to contact keycloak service host (sync workflow)
KEYCL_SVC_URL="https://legolas/auth/"

# keycloak realm, which manages objects for this turnkey deyployment. used by keycloak extension api 
#KEYCL_REALM="realm${GLOBAL_INSTANCE_SUFFIX}"
KEYCL_REALM="realm-LEGOLAS-dev"

# swagger docs? alex email 211207
KEYCL_JAVA_OPTS_APPEND="-Dkeycloak.profile.feature.upload_scripts=enabled -Dkeycloak.profile.feature.admin_fine_grained_authz=enabled -Dkeycloak.profile.feature.token_exchange=enabled"

# keycloak api container wildfly admin console
KEYCL_POD_EX_PORT_HTTP_ADMCONSOLE="9991"

# -----------------------------------------------------------------------------
# middleware pod configuration
# -----------------------------------------------------------------------------
# follow the steps below:
# (1) find a place where adc middleware should be installed, we use /var/data on our host
# $ git clone https://github.com/ireceptorplus-inesctec/adc-middleware.git  
# adjust permissions so that the running user account can read and write in adc-middleware
# (2) cd into adc-middleware
# copies example content to working folder
# $ cp -r example/ working
# make a copy of the properties file (example.properties) for your own settings
# $ cp working/config/middleware/example.properties working/config/middleware/middleware.properties
# in section below, set MIDWARE_CONFIG_FOLDER if you did not use "working" as folder name
# in file middleware.properties
# change at least:
# -- adc.resourceServerUrl - FQDN to your adc compliant repository, adjust host part
# -- server.port - change value to 8080
# -- app.resourceAllowedOrigins="" - add that configuration property
# -- uma.wellKnownUrl  - adjust host and port where your keycloak service is available (host and port)
# -- spring.redis.host - adjust value to "localhost" (runs in same pod)
# (4) read configuration below and adjust values if required for your deployment 

# if you copied example to working do not change value  
MIDWARE_CONFIG_FOLDER="working/config"

# uses same file path than example.properties
# if you did like before dont change value
MIDWARE_PROPS_FILE="middleware.properties"

# dont change the following five values

# added to adc-middleware and keycloak pod and container names
MIDWARE_INSTANCE_SUFFIX=$GLOBAL_INSTANCE_SUFFIX

# makes api container unique for this installation
MIDWARE_SVC_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# suffix to specify middleware db container insance e.g. TEST, DEV, PROD
MIDWARE_DB_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# name suffix for redis container
MIDWARE_REDIS_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# name suffix for keycloak extension api
MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX=$MIDWARE_INSTANCE_SUFFIX

# TAG for built svc  container https://hub.docker.com/r/irpinesctec/adc-middleware/tags
#MIDWARE_SVC_CONT_TAG="1.8.0"
#Successfully tagged localhost/adc-middleware:1.8.0-dkfz
#MIDWARE_SVC_CONT_TAG="dev"
#MIDWARE_SVC_CONT_TAG="1.8.0-dkfz"
#MIDWARE_SVC_CONT_TAG="1.9.2"
#MIDWARE_SVC_CONT_TAG="1.9.4"
MIDWARE_SVC_CONT_TAG="1.9.5"

# TAG for built db container (postgres)
MIDWARE_DB_CONT_TAG="12"

# service name for middleware pod
MIDWARE_POD_NAME_SVC="adc-middleware${KEYCL_INSTANCE_SUFFIX}"

# password for middleware db user postgres
MIDWARE_DB_PASSWORD="password2"

# host port of middleware postgres db, if postgres keycloak db runs on host port 5432, use 5333 here
MIDWARE_POD_EX_PORT_DB="5433"

# endpoint for protected adc api, e.g. for unprotected 8445 use 9445 for development deployment
MIDWARE_POD_EX_PORT_HTTP="9443"

# establishes trust relationship with keycloak service
MIDWARE_UMA_CLIENT_SECRET="8665a4ee-c8d0-4151-a79c-a057537fa24a"

# https://hub.docker.com/_/redis?tab=tags, leave empty or latest 
MIDWARE_REDIS_CONT_TAG="latest"

# tag for keycloakextensionapi 
# https://hub.docker.com/r/irpinesctec/keycloak_extension_api/tags
#MIDWARE_KEYCL_EXT_API_CONT_TAG="0.4.0"
#MIDWARE_KEYCL_EXT_API_CONT_TAG="0.6.0"
MIDWARE_KEYCL_EXT_API_CONT_TAG="0.7.0"

# host name - to connect to adc repository, is used id middleware.properties
MIDWARE_ADCREPO_HOST="legolas"

# ip adress of adc repository host, cannot be 127.0.0.1, is set at end of file
MIDWARE_ADCREPO_IP="127.0.0.1"

# normally use the one from the Docker compose file
# "Successfully tagged localhost/adc-middleware:1.8.0-dkfz"
# only set if local custom image should be used otherwise leave empty and default image will be pulled from repository
MIDWARE_IMAGE=""
#MIDWARE_IMAGE="localhost/adc-middleware"

# Passing additional Java specific properties. For example, increasing Java VM's memory. -Djavax.net.debug=all
MIDWARE_JAVA_EXTRA_OPTS=""

# external keycloak extension api port (intern gunicorn 8000), used by mw container, in 0.6.0 its 5000 see install script
MIDWARE_KCLEXT_EX_PORT_HTTP="9000"

# when using custom certificates in trust path, use this path to access keystore file in conatainer
# openjdk
#MIDWARE_KEYSTORE_PATH="/usr/local/openjdk-11/lib/security"
# bellsoft jdk
#MIDWARE_KEYSTORE_PATH="/usr/lib/jvm/jdk-17.0.3.1-bellsoft-x86_64/lib/security"
MIDWARE_KEYSTORE_PATH="/usr/lib/jvm/jdk/lib/security"

# -----------------------------------------------------------------------------
# acl-dashboard configuration
# -----------------------------------------------------------------------------
# you have to create the image yourself
# -- git clone https://github.com/ireceptorplus-inesctec/acl-dashboard.git
# -- cd acl-dashboard
# -- configure dashboard image
# -- podman build --tag acl-dashboard:laatest -f ./Dockerfile

# external port for acl-dashboard
ACLDASH_POD_EX_PORT="9091"

# internal port acl -dashboard is listening on see Dockerfile, cannot be 8080
ACLDASH_CONT_EXPOSE_PORT="8081"

# name of image: no value uses localhost/acl-dashboard, otherwise image will be downloaded from given url
# ACLDASH_IMAGE="localhost/acl-dashboard"
ACLDASH_IMAGE=""

# image tag dont use 'latest' tag for a local image https://stackoverflow.com/questions/63970144/podman-play-does-not-pull-localhost-image-get-https-localhost-v2-dial-tcp 
# ACLDASH_IMAGE_TAG="release15"
ACLDASH_IMAGE_TAG="release27"

# https://ireceptorplus.inesctec.pt/auth/
# see uma.wellKnownUrl in midware.properties, requests will go thru nginx
ACLDASH_VUE_APP_KEYCLOAK_URL="https://legolas/auth/"

# keycloak realm, do not change, is already set
ACLDASH_VUE_APP_KEYCLOAK_REALM=${KEYCL_REALM} 

# keycloak client id fo that realm
ACLDASH_VUE_APP_KEYCLOAK_CLIENT_ID="acl-dashboard"

# midware url in container
ACLDASH_VUE_APP_MIDDLEWARE_URL="https://legolas/"

# middleware resource endpoint
ACLDASH_VUE_APP_MAPPINGS_BASE_PATH="resource/"

# keycloak authz endpoint
ACLDASH_VUE_APP_AUTHZ_BASE_PATH="authz/"

# see  acl-dashboard/docker-compose.yml 
ACLDASH_VUE_APP_BACKEND_URL="dashboard_acl_backend/"

# # file name of script to run after acl-dashboard container was started, see release-23
# ACLDASH_POST_INSTALL_SCRIPT="replace_env_constants_in_JS.sh";

# -----------------------------------------------------------------------------
# acl-middleware-frontend (demo) runs in seperate pod
# https://github.com/Ross65536/adc-middleware-frontend
# localhost/adc-middleware-frontend                           release-2020-0706

# external port for frontend
ACLFRONT_POD_EX_PORT="9092"

# internal nginx port for container, not used is fixed to tcp 80 in nginx image
ACLFRONT_CONT_EXPOSE_PORT="80"

# image name
ACLFRONT_IMAGE="localhost/adc-middleware-frontend"

# image tag
ACLFRONT_IMAGE_TAG="release-2020-0706"

# pod name for frontend
ACLFRONT_POD_NAME_SVC="acl-middleware-frontend${GLOBAL_INSTANCE_SUFFIX}"

# -----------------------------------------------------------------------------
# service account id running the pod 
POD_USER_ID="1000"

# existing file will trigger db backup
# created by load_ and update_ scripts
DO_BKUP_FLAG=${PATH_BKUP_DIR}/${POD_NAME_SVC}/BKUP_DB.FLAG
# keycloaks backup path
DO_BKUP_FLAG_KEYCL=${PATH_BKUP_DIR}/${KEYCL_POD_NAME_SVC}/BKUP_DB.FLAG
# middlewares backup path
DO_BKUP_FLAG_MIDWARE=${PATH_BKUP_DIR}/${MIDWARE_POD_NAME_SVC}/BKUP_DB.FLAG


# -----------------------------------------------------------------------------
SCRIPT_DIR=`dirname "$0"`

CURRENTUSER=$(who | awk 'NR==1{print $1}')
#echo $CURRENTUSER

LOGFILE_NAME="adcauth_${host_name}_${KEYCL_POD_NAME_SVC}_.log"
if [ ! -e $SCRIPT_DIR/$LOGFILE_NAME  ];then
	touch $SCRIPT_DIR/$LOGFILE_NAME
	#chown $CURRENTUSER:$CURRENTUSER $SCRIPT_DIR/$LOGFILE_NAME
fi

function log () {
	local msg=$1
	echo $msg
	local dt=`date +%F_%T`
	echo "$dt $msg" >> $SCRIPT_DIR/$LOGFILE_NAME
}

function wait () {
	arg1=$1
	arg2=$2
	secs=$arg2
	#secs=$((5 * 60))
	while [ $secs -gt 0 ]; do
   		echo -ne "wait $arg2 secs (${arg1}) ... $secs\033[0K\r"
   		sleep 1
   		: $((secs--))
	done
}

function myip () {
	#IP_LAN_DEVICE="vpn0" see above
	current_IPv4_Lan=$(ip -o -4 addr list $IP_LAN_DEVICE | awk '{print $4}' | cut -d/ -f1)
	echo $current_IPv4_Lan
}

log "---"
echo "Using $(myip) as keycloak host ip."
KEYCL_HOST_IP=$(myip)
KEYCL_DB_IP=$(myip)
MIDWARE_ADCREPO_IP=$(myip)
