# Scripts for running adc-middleware containers with podman

Information about ac-middleware can be found here:

* https://ireceptorplus.inesctec.pt/wiki/
* https://github.com/ireceptorplus-inesctec/adc-middleware

The subfolders contain scripts for different versions of adc-middleware

* master-1-8-0
* master-1-7-1
* master-1-7-0
* master-test150
* ..

The scripts will create two pods, a keycloak server and the middleware service. The pods will run rootless, all communication goes thru hosts port > 1024, see project slirp4netns  (https://github.com/rootless-containers/slirp4netns)

List of containers: 

* keycloak pod: keycloak (application), keycloak_db (postgres database)
* middleware pod: middleware (aaplication), middleware_db (postgres database), keycloak extension api, redis 


## Installation

tested with operating systems fedora 34, centos 8.2 and alma linux 8.4
- fedora 34,35,36 (podman 4.1.1)
- centos 8.2
- alma linux 8.5,8.5 (podman 4.0.2)

(1) prepare deployment

- Find a place where you want to install adc-middleware, e.g. /var/data
- pull from github 
* https://github.com/ireceptorplus-inesctec/adc-middleware
* https://github.com/ireceptorplus-inesctec/acl-dashboard
* this repository

- copy the files from master-1-7-1 (or newer version) to the acl-middleware folder 
- cd adc-middleware && ln -s master-1-7-1  scriptspod 
- make a working copy of the deplyoment folder cp -R deployment/ working (previous folder name was example)
- copy custom field mapping file to working/config  cp src/main/resources/field-mapping.csv working/config/ 
  note: file is not used by application, use acl-dashboard application to assign fields to scopes
- copy required custom certificates (for trust relationship) to working/config 
- copy middleware.properties to working/config/middleware
- make a plan for the deployment of the containers and pods
	* an adc repository must already be running in the background and not be accessible from unauthenticated users
	* one host for all pods or seperated pods?
	* nginx configuration
	* required certificates (if private ca is used and trust for root certificates needs to be established)
	* required open firewall ports
	* id adresses, hostnames, ..
- edit file scriptspod/podman-adcauth.conf.sh and adjust values according to your plan

(2) install keycloak pod

(3) configure IdP backend for keycloak (users from that backend can have access)

(3) build acl-dashboard

(4) install middleware pod

(5) determine which studies should be accessible who "owns" (controls access) each study, then synchronize adc repository and middleware

(6) test access to protected studies

## version master-1-8-0 and forward (outgoing) proxy support, if needed
The following is NOT required when using a version after development branch state from 2022-02-15. If you are using that version,
see file middleware.properties and add proxy configuration in that file.

If using version 1.8.0:
In case your deplyoment is behind an outgoing proxy server you have to build your own custom image. 
Rename file HttpFacade-PROXY_PATCH.java to HttpFacade.java and put it to folder src/main/java/pt/inesctec/adcauthmiddleware/http
This is a patch to https://github.com/ireceptorplus-inesctec/adc-middleware/releases/tag/1.8.0
 
In line 26 specify proxy host and port. 
Then build a custom adc-middleware container image with

```
podman build --tag adc-middleware:1.8.0-dkfz -f ./Dockerfile
```

Then edit scriptspod/podman-adcauth.conf.sh, add the image name
MIDWARE_IMAGE="localhost/adc-middleware"

Instead of pulling the image from docker hub the local image will be used.

## master-1.9.5
jdk changed to belsoft Liberica JDK
MIDWARE_KEYSTORE_PATH is used to configure path to trustore file (jdk dependend)

adc-middleware -> 1.9.5
keycloak_extension_api -> 0.7.0

## podman version 4.1.1
All --add-host directives moved to pod create command.

## Backup and Restore for the postgres databases

crontab -e

```
# adc-middleware bkups
18 2 * * * touch /var/data/bkup/adc-middleware-ADC-prod/BKUP_DB.FLAG && /var/data/adc-middleware/scriptspod/backup_database_middleware.sh >/dev/null 2>&1
20 2 * * * touch /var/data/bkup/adc-keycloak-ADC-prod/BKUP_DB.FLAG && /var/data/adc-middleware/scriptspod/backup_database_keycloak.sh >/dev/null 2>&1
22 2 * * * /var/data/adc-middleware/scriptspod/bkup_file_rotation_middleware.sh && /var/data/adc-middleware/scriptspod/bkup_file_rotation_keycloak.sh >/dev/null 2>&1
```
Addjust the paths according to your deployment.
  
