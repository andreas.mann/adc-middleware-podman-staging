# acl-dashboard configuration with adjustments for container deployment with podman

see https://github.com/ireceptorplus-inesctec/acl-dashboard

## file .env


-    VUE_APP_KEYCLOAK_URL: Keycloak's base URL (http://localhost:8082/auth/)
-    VUE_APP_KEYCLOAK_REALM: Keycloak's realm (master)
-    VUE_APP_KEYCLOAK_CLIENT_ID: Keycloak's dashboard client id (acl-dashboard)
-    VUE_APP_MIDDLEWARE_URL: ADC Middleware's base URL (http://localhost:8080/)
-    VUE_APP_MAPPINGS_BASE_PATH: ADC Middleware's field mappings base path (resource/)
-    VUE_APP_AUTHZ_BASE_PATH: ADC Middleware's dashboard backend base path (authz/)


## file Dockerfile
port to expose has been changed



