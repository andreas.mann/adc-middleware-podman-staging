#!/bin/bash
#
# installs middleware service as pod from adc-middleware 
# see https://github.com/ireceptorplus-inesctec/adc-middleware
#
# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh" 

if [ ! -f "$SCRIPT_DIR/$POD_CONF_FILE" ]; then
    echo "The file $POD_CONF_FILE does not exist. Edit file podman-adcauth.conf.sh.EDIT then remove .EDIT"
	exit
fi

# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE 

echo "CHECK: If acl-dashboard is used: image built (script will run $ACLDASH_IMAGE:$ACLDASH_IMAGE_TAG) ?"
podman images | grep $ACLDASH_IMAGE
echo "all ok: y/n?"
read input
if [ $input != "y"  ]; then 
    exit
fi


#echo --- BIN_PODMAN  ---  $PODMAN_CMD
log "[$MIDWARE_POD_NAME_SVC installation]"
log "pod service name: $MIDWARE_POD_NAME_SVC"
log "pod external port: $MIDWARE_SVC_PORT"
log "user id running db container: $POD_USER_ID"

# required commands
commands=("podman" "curl" "readlink" "buildah");
for i in "${commands[@]}"
do
	if ! [ $(command -v $i) > 0 ];then
		log "	$i command could not be found, install first. "
		exit
	else
		log "	$i command available"		
	fi
done

# db data folder
MIDWARE_DB_DATA_DIR=".midware_db_data"
log "create $MIDWARE_DB_DATA_DIR and bkup folders"
bkup_folder_list=("incoming" "backup.daily" "backup.weekly" "backup.monthly" "restore");
if [ ! -e $MIDWARE_DB_DATA_DIR  ];then

	mkdir $MIDWARE_DB_DATA_DIR >> $SCRIPT_DIR/$LOGFILE_NAME 
	podman unshare chown $POD_USER_ID:$POD_USER_ID -R $MIDWARE_DB_DATA_DIR 

	mkdir -p ${PATH_BKUP_DIR}/$KEYCL_POD_NAME_SVC
    for b in "${bkup_folder_list[@]}"
    do  
        mkdir -p ${PATH_BKUP_DIR}/$MIDWARE_POD_NAME_SVC/$b >> $SCRIPT_DIR/$LOGFILE_NAME
        #
    done

else
	log "	data directory for db already exists"
	exit
fi

# redis db data folder
MIDWARE_DB_REDIS_DATA_DIR=".midware_db_redis_data"
log "create $MIDWARE_DB_REDIS_DATA_DIR"
if [ ! -e $MIDWARE_DB_REDIS_DATA_DIR  ];then

    mkdir $MIDWARE_DB_REDIS_DATA_DIR >> $SCRIPT_DIR/$LOGFILE_NAME 
    podman unshare chown $POD_USER_ID:$POD_USER_ID -R $MIDWARE_DB_REDIS_DATA_DIR 
else
    log "   data directory for db redis already exists"
    exit
fi

log "check for existing pod.."
podman pod exists $MIDWARE_POD_NAME_SVC 
if [ $? -eq 0  ]; then
	log "	pod with that name already exists."
	exit
fi

# --- pull containers ---------------------------------------------------------
log "### pull containers ------------------------------------------------------"
# build container image
# https://hub.docker.com/r/jboss/keycloak/tags
# docker pull jboss/keycloak:15.0.0
# Dockerfile
# FROM jboss/keycloak:15.0.0
# COPY ./js-policies /opt/jboss/keycloak/standalone/deployments
# podman pull registry.hub.docker.com/jboss/keycloak:${KEYCL_SVC_CONT_TAG} 
# docker pull irpinesctec/adc-middleware:1.5.0
# docker pull irpinesctec/keycloak_extension_api:latest
if [ -z "$MIDWARE_IMAGE" ]
then
        #echo "\$var is empty"
        podman pull registry.hub.docker.com/irpinesctec/adc-middleware:$MIDWARE_SVC_CONT_TAG
		MIDWARE_IMAGE="registry.hub.docker.com/irpinesctec/adc-middleware"
		echo "using default image $MIDWARE_IMAGE"
else
        #echo "\$var is NOT empty"
        echo "use local image $MIDWARE_IMAGE"
fi

sleep 2

podman pull docker.io/library/postgres:$MIDWARE_DB_CONT_TAG
sleep 2

podman pull docker.io/redis:$MIDWARE_REDIS_CONT_TAG
sleep 2

podman pull registry.hub.docker.com/irpinesctec/keycloak_extension_api:$MIDWARE_KEYCL_EXT_API_CONT_TAG
sleep 2

# --- pod create --------------------------------------------------------------

log "### creating the adc-middleware pod --------------------------------------"
podman pod create --name $MIDWARE_POD_NAME_SVC --share net -p $MIDWARE_POD_EX_PORT_HTTP:8080 -p $MIDWARE_POD_EX_PORT_DB:5432 -p $ACLDASH_POD_EX_PORT:$ACLDASH_CONT_EXPOSE_PORT

# --- start and add containers to pod ----------------------------------------- 

log "add container redis"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			-v $MIDWARE_DB_REDIS_DATA_DIR:/data:Z \
			--name=middleware-redis$MIDWARE_REDIS_CONT_NAME_SUFFIX \
			docker.io/library/redis:$MIDWARE_REDIS_CONT_TAG

wait "run redis" 3

log "add keycloac extension api"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			--add-host=${KEYCL_DB_HOSTNAME}:${KEYCL_DB_IP} \
			-e DB_DATABASE="keycloak_db" \
			-e DB_USER="postgres" \
			-e DB_PASSWORD=$KEYCL_DB_PASSWORD \
			-e DB_HOST=$KEYCL_DB_HOSTNAME \
			-e DB_PORT=$KEYCL_POD_EX_PORT_DB \
			-e KEYCLOAK_URL=$KEYCL_SVC_URL \
			-e REALM=$KEYCL_REALM \
			-e PYTHONHTTPSVERIFY=0 \
			-e REQUESTS_CA_BUNDLE=/etc/ssl/certs/ca-certificates.crt \
			-v ./${MIDWARE_CONFIG_FOLDER}:/config:Z \
			--name=keycloakextensionapi$MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX \
			registry.hub.docker.com/irpinesctec/keycloak_extension_api:$MIDWARE_KEYCL_EXT_API_CONT_TAG

wait "run keycl ext api" 3

log "add container postgres (midware_db)"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			-e POSTGRES_PASSWORD=$MIDWARE_DB_PASSWORD \
			-v $MIDWARE_DB_DATA_DIR:/var/lib/postgresql/data:Z \
			-v ${PATH_BKUP_DIR}/$MIDWARE_POD_NAME_SVC:/mnt/bkup:Z \
            --name=middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX} \
			docker.io/library/postgres:$MIDWARE_DB_CONT_TAG

wait "run postgres" 10

# create bkup folder chown not required 
# root@98106decdb80:/mnt/bkup# pg_dump -Fc keycloak_db -U postgres -h localhost > /mnt/bkup/incoming/postgres_legolas_adc-keycloak-LEGOLAS-dev_211206134500.sql.dump
podman exec -d --user root middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX} sh "cd /mnt && mkdir -p bkup && chown postgres bkup"

#log "add container adc-middleware service (add to container: dns ${KEYCL_HOST} with ${KEYCL_HOST_IP} ) "
log "add container adc-middleware service"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC \
			 --add-host=${KEYCL_HOST}:${KEYCL_HOST_IP} \
			 --add-host=${MIDWARE_ADCREPO_HOST}:${MIDWARE_ADCREPO_IP} \
			-e UMA_CLIENT_SECRET=$MIDWARE_UMA_CLIENT_SECRET \
			-e DB_PASSWORD=$MIDWARE_DB_PASSWORD \
			-e PROPERTIES_PATH="/config/middleware/${MIDWARE_PROPS_FILE}" \
			-e JAVA_EXTRA_OPTS=$MIDWARE_JAVA_EXTRA_OPTS \
			-v ./${MIDWARE_CONFIG_FOLDER}:/config:Z \
			-v ./src/main/resources/db/postgresql:/config/db/postgresql:Z \
            --name=middleware${MIDWARE_SVC_CONT_NAME_SUFFIX} \
			$MIDWARE_IMAGE:$MIDWARE_SVC_CONT_TAG

wait "run middleware" 20

log "add container acl-dashboard"
podman run --user $POD_USER_ID -d --pod=$MIDWARE_POD_NAME_SVC  \
			--add-host=${KEYCL_HOST}:${KEYCL_HOST_IP} \
			-e VUE_APP_KEYCLOAK_URL=$ACLDASH_VUE_APP_KEYCLOAK_URL \
			-e VUE_APP_KEYCLOAK_REALM=$ACLDASH_VUE_APP_KEYCLOAK_REALM \
			-e VUE_APP_KEYCLOAK_CLIENT_ID=$ACLDASH_VUE_APP_KEYCLOAK_CLIENT_ID \
			-e VUE_APP_MIDDLEWARE_URL=$ACLDASH_VUE_APP_MIDDLEWARE_URL \
			-e VUE_APP_MAPPINGS_BASE_PATH=$ACLDASH_VUE_APP_MAPPINGS_BASE_PATH \
			-e VUE_APP_AUTHZ_BASE_PATH=$ACLDASH_VUE_APP_AUTHZ_BASE_PATH \
			-e VUE_APP_BACKEND_URL=$ACLDASH_VUE_APP_BACKEND_URL \
			-e PORT=$ACLDASH_CONT_EXPOSE_PORT \
			-v ./${MIDWARE_CONFIG_FOLDER}:/config:Z \
            --name=acl-dashboard${MIDWARE_SVC_CONT_NAME_SUFFIX} \
			$ACLDASH_IMAGE:$ACLDASH_IMAGE_TAG

wait "run acl-dashboard" 6

# --- install certificates
# target inside adcmiddleware container /usr/local/share/ca-certificates
CERT_TARGET_PATH="/usr/local/share/ca-certificates"
if [ $GLOBAL_DO_UPDATE_CUSTOM_CERTIFICATES == "true" ]; then
	log "### install custom certificates to middleware container ..."
	for i in ${GLOBAL_CUSTOM_CERTIFICATES_LIST[@]}; do
		podman exec -d --user root middleware${MIDWARE_SVC_CONT_NAME_SUFFIX} sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
		sleep 1
		podman exec -d --user root acl-dashboard${MIDWARE_SVC_CONT_NAME_SUFFIX} sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
		sleep 1
		podman exec -d --user root keycloakextensionapi$MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
		sleep 1 && log "${i} installed to container os"
		podman exec -d --user root middleware${MIDWARE_SVC_CONT_NAME_SUFFIX} \
			 sh -c "cd /usr/local/openjdk-11/lib/security && keytool -keystore cacerts -storepass changeit -noprompt -trustcacerts -importcert -alias cacert -file /config/${i}"
		sleep 1 && log "${i} installed to jdk truststore"
	done
	sleep 2
	podman exec -d --user root middleware${MIDWARE_SVC_CONT_NAME_SUFFIX} sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
	sleep 1
	podman exec -d --user root acl-dashboard${MIDWARE_SVC_CONT_NAME_SUFFIX} sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
	sleep 1
	podman exec -d --user root keycloakextensionapi$MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
	log "... done" 
fi
sleep 2


# --- create systemd service --------------------------------------------------
log "### create systemd service -----------------------------------------------"
# launch on boot
# adc-middleware/pod-adc-auth-keycloak-test1.service
# adc-middleware/container-keycloak-test1.service
# adc-middleware/container-keycloak_db-test1.service

mkdir -p ~/.config/systemd/user/

#podman generate systemd --name $POD_NAME_SVC > ~/.config/systemd/user/pod-${POD_NAME_SVC}.service
#systemctl enable pod-${POD_NAME_SVC}.service --user
podman generate systemd --files --name $MIDWARE_POD_NAME_SVC

# pod service move file to user directory for systemd 
cp pod-${MIDWARE_POD_NAME_SVC}.service  ~/.config/systemd/user/pod-${MIDWARE_POD_NAME_SVC}.service
rm pod-${MIDWARE_POD_NAME_SVC}.service

# redis container
cp container-middleware-redis${MIDWARE_REDIS_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware-redis${MIDWARE_REDIS_CONT_NAME_SUFFIX}.service
rm container-middleware-redis${MIDWARE_REDIS_CONT_NAME_SUFFIX}.service

# kc extension api container
cp container-keycloakextensionapi${MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-keycloakextensionapi${MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX}.service
rm container-keycloakextensionapi${MIDWARE_KEYCL_EXT_API_CONT_NAME_SUFFIX}.service

# middleware_db container
cp container-middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX}.service
rm container-middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX}.service

# middleware service container
cp container-middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}.service
rm container-middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}.service

# acl-dashboard container
cp container-acl-dashboard${MIDWARE_SVC_CONT_NAME_SUFFIX}.service \
~/.config/systemd/user/container-acl-dashboard${MIDWARE_SVC_CONT_NAME_SUFFIX}.service
rm container-acl-dashboard${MIDWARE_SVC_CONT_NAME_SUFFIX}.service

wait "enable systemd service" 2
systemctl enable pod-${MIDWARE_POD_NAME_SVC}.service --user

log "systemd service files created and service enabled"
wait "processes to start" 10

# --- pod top -----------------------------------------------------------------

if [ $GLOBAL_DO_UPDATE_CUSTOM_CERTIFICATES == "true" ]; then
	echo "restarting service (making custom certificates available)"
	#systemctl --user stop pod-${MIDWARE_POD_NAME_SVC}.service 
	#podman stop middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}
	podman pod stop $MIDWARE_POD_NAME_SVC
	sleep 4
	podman pod start $MIDWARE_POD_NAME_SVC
	#podman start middleware${MIDWARE_SVC_CONT_NAME_SUFFIX}
	#systemctl --user start pod-${MIDWARE_POD_NAME_SVC}.service 

	#systemctl --user restart pod-${MIDWARE_POD_NAME_SVC}.service 
	wait "service starting ..." 20
fi

# display processes in pod
podman pod top $MIDWARE_POD_NAME_SVC

echo "adc-middleware service runs as a user service"
echo "check service : systemctl status pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "start service : systemctl start pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "stop service     : systemctl stop pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "disable service : systemctl disable pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo "enable service : systemctl enable pod-${MIDWARE_POD_NAME_SVC}.service --user"
echo 

#echo "end of installation for adc-middleware"
log "End of adc-middleware installation, next step configure integration with keycloak"

