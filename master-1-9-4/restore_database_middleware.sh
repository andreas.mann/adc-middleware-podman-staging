#!/bin/bash
SCRIPT_DIR=`dirname "$0"`

restore_abs_file_path=$1
parent_dir="$(dirname "$restore_abs_file_path")"
base_name="$(basename "$restore_abs_file_path")"

#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh"
# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE

# pg_restore -d dbname filename

# --log-level debug 
# -u root \
podman exec \
            -i \
			-e MidwareDbName="postgres" \
            -e BASE_NAME=$base_name \
            middleware_db${MIDWARE_DB_CONT_NAME_SUFFIX} \
            sh -c 'pg_restore -Fc -d $MidwareDbName -U postgres -h localhost --clean < /mnt/bkup/restore/$BASE_NAME'

log "restore database: ${restore_abs_file_path}"

