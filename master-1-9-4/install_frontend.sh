#!/bin/bash
#
# installs middleware demo frontend as pod 
# see https://github.com/Ross65536/adc-middleware-frontend.git
#
# -----------------------------------------------------------------------------

SCRIPT_DIR=`dirname "$0"`
#SCRIPT_DIR_FULL="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_DIR_FULL="$( readlink -f ${SCRIPT_DIR}  )";
POD_CONF_FILE="podman-adcauth.conf.sh" 

if [ ! -f "$SCRIPT_DIR/$POD_CONF_FILE" ]; then
    echo "The file $POD_CONF_FILE does not exist. Edit file podman-adcauth.conf.sh.EDIT then remove .EDIT"
	exit
fi

# read configuration for turnkey pod
. $SCRIPT_DIR_FULL/$POD_CONF_FILE 

echo "CHECK: If acl-middleware-frontend is used: image built (script will run $ACLFRONT_IMAGE:$ACLFRONT_IMAGE_TAG) ?"
podman images | grep $ACLFRONT_IMAGE
echo "CHECK: Is keycloak service prepared with front-end client and has image been built with that config in /public/keycloak.json?"
echo "all ok: y/n?"
read input
if [ $input != "y"  ]; then 
    exit
fi


#echo --- BIN_PODMAN  ---  $PODMAN_CMD
log "[acl-middleware-frontend  installation]"
log "pod service name: $ACLFRONT_POD_NAME_SVC"
log "pod external port: $ACLFRONT_POD_EX_PORT"
log "user id: $POD_USER_ID"

# required commands
commands=("podman" "curl" "readlink" "buildah");
for i in "${commands[@]}"
do
	if ! [ $(command -v $i) > 0 ];then
		log "	$i command could not be found, install first. "
		exit
	else
		log "	$i command available"		
	fi
done

# db data folder
# none

# redis db data folder
# none 

log "check for existing pod.."
podman pod exists $ACLFRONT_POD_NAME_SVC 
if [ $? -eq 0  ]; then
	log "	pod with that name already exists."
	exit
fi

# --- pull containers ---------------------------------------------------------
log "### pull containers ------------------------------------------------------"
# nothing to pull

# --- pod create --------------------------------------------------------------

log "### creating the adc-middleware-frontend pod --------------------------------------"
podman pod create --name $ACLFRONT_POD_NAME_SVC --share net -p $ACLFRONT_POD_EX_PORT:8080 

# --- start and add containers to pod ----------------------------------------- 

log "add container frontend"
podman run --user $POD_USER_ID -d --pod=$ACLFRONT_POD_NAME_SVC \
			-v ./${MIDWARE_CONFIG_FOLDER}:/config:Z \
			--name=middleware-frontend${GLOBAL_INSTANCE_SUFFIX} \
			$ACLFRONT_IMAGE:$ACLFRONT_IMAGE_TAG

wait "run acl-dashboard" 6

# --- install certificates
# target inside adcmiddleware container /usr/local/share/ca-certificates
CERT_TARGET_PATH="/usr/local/share/ca-certificates"
if [ $GLOBAL_DO_UPDATE_CUSTOM_CERTIFICATES == "true" ]; then
    log "### install custom certificates to middleware-frontend container ..."
    for i in ${GLOBAL_CUSTOM_CERTIFICATES_LIST[@]}; do
        podman exec -d --user root middleware-frontend${GLOBAL_INSTANCE_SUFFIX} sh -c "cd ${CERT_TARGET_PATH} && cp /config/${i} ${CERT_TARGET_PATH} && chmod 644 ${i}"
        sleep 1
    done
    sleep 1
    podman exec -d --user root middleware-frontend${GLOBAL_INSTANCE_SUFFIX} sh -c "cd ${CERT_TARGET_PATH} && /usr/sbin/update-ca-certificates"
    log "... done" 
fi
sleep 2


# --- create systemd service --------------------------------------------------
log "### create systemd service -----------------------------------------------"
# launch on boot
# adc-middleware/pod-adc-auth-keycloak-test1.service
# adc-middleware/container-keycloak-test1.service
# adc-middleware/container-keycloak_db-test1.service

mkdir -p ~/.config/systemd/user/

#podman generate systemd --name $POD_NAME_SVC > ~/.config/systemd/user/pod-${POD_NAME_SVC}.service
#systemctl enable pod-${POD_NAME_SVC}.service --user
podman generate systemd --files --name $ACLFRONT_POD_NAME_SVC

# pod service move file to user directory for systemd 
cp pod-${ACLFRONT_POD_NAME_SVC}.service  ~/.config/systemd/user/pod-${ACLFRONT_POD_NAME_SVC}.service
rm pod-${ACLFRONT_POD_NAME_SVC}.service

# frontend container
cp container-middleware-frontend${GLOBAL_INSTANCE_SUFFIX}.service \
          ~/.config/systemd/user/container-middleware-frontend${GLOBAL_INSTANCE_SUFFIX}.service
rm container-middleware-frontend${GLOBAL_INSTANCE_SUFFIX}.service

wait "enable systemd service" 2
systemctl enable pod-${ACLFRONT_POD_NAME_SVC}.service --user

log "systemd service files created and service enabled"

wait "processes to start" 10

# --- pod top -----------------------------------------------------------------

# log "running processes in pod"
podman pod top $ACLFRONT_POD_NAME_SVC

#podman pod stop $MIDWARE_POD_NAME_SVC

#wait "stopping running pod and start systemd service" 30

#systemctl start pod-${MIDWARE_POD_NAME_SVC}.service --user
#wait "service starting ..." 15

echo "adc-middleware-frontend service runs as a user service"
echo "check service : systemctl status pod-${ACLFRONT_POD_NAME_SVC}.service --user"
echo "start service : systemctl start pod-${ACLFRONT_POD_NAME_SVC}.service --user"
echo "stop service     : systemctl stop pod-${ACLFRONT_POD_NAME_SVC}.service --user"
echo "disable service : systemctl disable pod-${ACLFRONT_POD_NAME_SVC}.service --user"
echo "enable service : systemctl enable pod-${ACLFRONT_POD_NAME_SVC}.service --user"
echo 

#echo "end of installation for adc-middleware"
log "End of adc-middleware-frontend installation."

